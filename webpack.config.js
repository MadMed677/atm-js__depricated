'use strict';

const webpack = require('webpack');

module.exports = {

    entry: {
        app: ['webpack-dev-server/client', './app/app.js'],
        test: './test/testSpec.js'
    },

    output: {
        path: __dirname + '/dist',
        publicPath: '/',
        filename: "[name].js"
    },

    module: {
        loaders: [
            {
                test: /\.js/,
                loaders: ['jsx', 'babel'],
                exclude: /node_modules/
            }
        ]
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ],

    devServer: {
        host: 'localhost',
        port: '8080'
    },

    devtool: 'source-map'

};
