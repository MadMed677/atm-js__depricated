import React                    from 'react';
import ReactDOM                 from 'react-dom';

import App                      from './main/app.react';


ReactDOM.render(
    <App />,
    document.getElementById('root')
);