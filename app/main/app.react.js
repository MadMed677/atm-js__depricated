import React                from 'react';
import ATM                  from './atm/ATM';

import ATMNominal           from './atm/atm-nominal.react';
import ATMUserNominal       from './atm/atm-user-nominal.react';
import ATMGetCache          from './atm/atm-get-cash.react.js';

export default class App extends React.Component {
    constructor() {
        super();

        this.state = { nominalValue: ATM.values, userValue: [] };
    }

    componentWillMount() {
        Object.observe(ATM.values, () => this.setState({ nominalValue: ATM.values }));
    }

    updateUserValue(userValues) {
        let result = [ ...this.state.userValue ];

        if ( !result.length ) return userValues;

        let found = false;
        _.each(userValues, uval => {
            _.each(result, lval => {
                if ( lval.value == uval.value ) {
                    lval.count += uval.count;


                    found = true;
                }
            });

            if ( !found ) {
                result.push({ value: uval.value, count: uval.count });
            }
        });

        return result;
    }

    getCash(amount, e) {
        e.preventDefault();

        if ( !amount ) return;

        amount = parseFloat(amount);
        const res = ATM.getCash(amount);

        if ( res.status ) {
            this.setState({ nominalValue: res.data, userValue: this.updateUserValue(res.user) });
        } else {
            console.warn(res.error);
        }
    }

    render() {
        const nominalList = this.state.nominalValue.map( (item, i) =>
            <ATMNominal item={ item } key={ `nominal-${i}` } />
        );

        const userList = this.state.userValue.map( (item, i) =>
            <ATMUserNominal item={ item } key={`user-nominal-${i}`} />
        );

        return (
            <div className="container">
                <h1>The ATM</h1>
                <hr/>

                <h2>What Have ATM</h2>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Nominal</th>
                            <th>Count</th>
                        </tr>
                    </thead>
                    <tbody>
                        { nominalList }
                    </tbody>
                </table>
                <div>
                    <ATMGetCache getCash={ this.getCash.bind(this) } />
                </div>

                <hr/>

                <h2>What You Have</h2>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Nominal</th>
                            <th>Count</th>
                        </tr>
                    </thead>
                    <tbody>
                        { userList }
                    </tbody>
                </table>
            </div>
        );
    }
}
