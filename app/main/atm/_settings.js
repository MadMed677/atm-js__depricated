export default {
    nominalValue: [
        { value: 5000, count: 20 },
        { value: 1000, count: 100 },
        { value: 500, count: 1000 },
        { value: 100, count: 1000 },
    ]
};