import React                from 'react';
import ReactDOM             from 'react-dom';

export default class ATMGetCash extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <form className="row" onSubmit={ e => this.props.getCash(ReactDOM.findDOMNode(this.refs.cash).value, e) }>
                <div className="col-xs-6">
                    <input type="text" className="form-control" ref="cash"/>
                </div>
                <div className="col-xs-6">
                    <button type="button"
                            className="btn btn-primary"
                            onClick={ e => this.props.getCash(ReactDOM.findDOMNode(this.refs.cash).value, e) }>
                        Get Cache
                    </button>
                </div>
            </form>
        );
    }
}