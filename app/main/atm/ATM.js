import _                        from 'lodash';
import Settings                 from './_settings';

// Create private prop
let _values = Settings.nominalValue;

/**
 * Create ATM class
 */
export default class ATM {
    constructor() {}

    static get values() { return _values }

    static set values(val) { _values = val }

    static getCash(amount = 0) {
        // Sort on desc
        let cash = _values.sort( (a, b) => a.value < b.value);

        let i = 0;
        let canGiveMoney = false;
        let result = [];

        do {
            if ( !cash[i] ) return { status: false, error: 'Haven\'t anought money' };

            const major = parseInt(amount / cash[i].value);

            if ( major == 0 ) { i += 1; continue; }

            const cashAmount = major * cash[i].value;

            cash[i].count -= major;

            result.push({ value: cash[i].value, count: major });

            if ( cash[i].count <= 0 ) {
                return { status: false, error: 'Haven\'t anought money' };
            }

            amount -= cashAmount;

            // If we can give all money
            if ( amount == 0 ) {
                canGiveMoney = true;
                break;
            }

            i++;
            continue;
        } while ( !canGiveMoney );

        _values = cash;
        return { status: true, data: cash, user: result };
    }
};