import React                from 'react';

export default class ATMNominal extends React.Component {
    constructor() {
        super();
    }

    render() {
        const { item } = this.props;

        return (
            <tr>
                <td>{ item.value }<i className="fa fa-rub"></i></td>
                <td>{ item.count }</td>
            </tr>
        );
    }
}