# Реализация простого банкомата

## Установка

```
$ npm install
$ webpack
$ webpack-dev-server
```

Проект будет доступен по ```localhost:8080```

Тесты находятся в папке ```test/index.html```
