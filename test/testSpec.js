import ATM                  from '../app/main/atm/ATM';

describe('ATM', () => {
    const cash = 5700;
    describe(`Get ${cash} cash`, () => {

        it('Equal: any object', () => {
            const money = ATM.getCash(cash);
            expect(money).toEqual( jasmine.any(Object) );
        });

        it('Equal: objectContaining array', () => {
            const money = ATM.getCash(cash);

            expect(money).toEqual( jasmine.objectContaining({
                user: jasmine.any(Array)
            }));
        });

        it(`Have: ${cash} cash user`, () => {
            const money = ATM.getCash(cash);
            let result = 0;

            money.user.forEach( i => result += i.value * i.count);

            expect(cash).toEqual(result, `User haven't ${cash} cash`);
        });

        it(`Bank give ${cash} cash to user`, () => {
            let total = 0;
            ATM.values.forEach( i => total += i.value * i.count);

            const money = ATM.getCash(cash);
            let result = 0;

            money.data.forEach( i => result += i.value * i.count);

            expect(cash).toEqual(total - result, `Bank didn't give ${cash} to user`);
        });
    })
});